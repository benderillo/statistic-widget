/**
 * 
 */
package com.android.littleutilities.easystats;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
/**
 * @author alex.veprik
 *
 */
public class StatisticWidgetProvider extends AppWidgetProvider
{
    private final static String TAG = "StatisticWidgetProvider";
    private ComponentName mThisWidget = null;
    private AppWidgetManager mWManager = null;
    private RemoteViews mRemoteViews = null;
    private Context mContext = null;
    
    private final int MSG_WIDGET_UPDATE = 0;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_WIDGET_UPDATE) {
                try {
                    // launch the service (if it is already running the start-up request will be ignored)
                    Log.e(TAG, "start processing MSG_WIDGET_UPDATE");
                    mContext.startService(new Intent(mContext, StatisticDataProvider.class));
                }catch(Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    };
    
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive " + intent);
        super.onReceive(context, intent);

        String action = intent.getAction();
        if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
            //onUpdate();
        } else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_DELETED)) {
            //onDeleted();
        } else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_ENABLED)) {
            //onEnabled();
        } else if (action.equals(AppWidgetManager.ACTION_APPWIDGET_DISABLED)) {
            //onDisabled();
        } else if (action.equals(StatisticDataProvider.ACTION_AMALGAMATED_UPDATE)) {
            mThisWidget = new ComponentName(context, StatisticWidgetProvider.class); 
            mWManager = AppWidgetManager.getInstance(context);
            mRemoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_portrait);

            
            //Data values
            mRemoteViews.setTextViewText(R.id.DataInCounter, Util.convertBytes(intent.getLongExtra("cRecvData", 0)));
            mRemoteViews.setTextViewText(R.id.DataOutCounter, Util.convertBytes(intent.getLongExtra("cSentData", 0)));

            //Out Call values
            mRemoteViews.setTextViewText(R.id.CallsOutCounter, Util.convertSeconds(intent.getIntExtra("dSentCall", 0)));
            mRemoteViews.setTextViewText(R.id.CallsOutNum, "(" + String.valueOf(intent.getIntExtra("cSentCall", 0)) + ")");

            //In call values
            mRemoteViews.setTextViewText(R.id.CallsInCounter, Util.convertSeconds(intent.getIntExtra("dRecvCall", 0)));
            mRemoteViews.setTextViewText(R.id.CallsInNum, "(" + String.valueOf(intent.getIntExtra("cRecvCall", 0)) + ")");
            
            //Mms
            mRemoteViews.setTextViewText(R.id.MmsInCounter, String.valueOf(intent.getIntExtra("cRecvMms", 0)));
            mRemoteViews.setTextViewText(R.id.MmsOutCounter, String.valueOf(intent.getIntExtra("cSentMms", 0)));

            //Sms
            mRemoteViews.setTextViewText(R.id.MessageInCounter, String.valueOf(intent.getIntExtra("cRecvSms", 0)));
            mRemoteViews.setTextViewText(R.id.MessageOutCounter, String.valueOf(intent.getIntExtra("cSentSms", 0)));

            Intent appIntent = new Intent(context, MainWidgetActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, appIntent, 0);

            mRemoteViews.setOnClickPendingIntent(R.id.main_frame, pendingIntent);

            mWManager.updateAppWidget(mThisWidget, mRemoteViews);
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        Log.e(TAG, "onDeleted");
        super.onDeleted(context, appWidgetIds);
    }
    
    public void onDisabled(Context context) {
        Log.e(TAG, "onDisabled");
        mHandler.removeMessages(MSG_WIDGET_UPDATE);
        super.onDisabled(context);
    }
    
    public void onEnabled(Context context) {
       Log.e(TAG, "onEnabled");
       mContext = context;
       super.onEnabled(context);
    }
    
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        Log.e(TAG, "onUpdate");
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        mContext = context;
        mHandler.sendEmptyMessageDelayed(MSG_WIDGET_UPDATE, 300);
        
        final int N = appWidgetIds.length;

        // Perform this loop procedure for each App Widget that belongs to this provider

        for (int i=0; i < N; i++) {
            int appWidgetId = appWidgetIds[i];
            // Create an Intent to launch ExampleActivity
            Intent intent = new Intent(context, MainWidgetActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            // Get the layout for the App Widget and attach an on-click listener to the button
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_portrait);
            views.setOnClickPendingIntent(R.id.main_frame, pendingIntent);
            // Tell the AppWidgetManager to perform an update on the current App Widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }
}
