package com.android.littleutilities.easystats;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.CallLog;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;


public class StatisticDataProvider extends Service
{    
    //Public Intents
    public static final String ACTION_SENT_SMS_UPDATE = "com.android.littleutilities.easystats.ACTION_SENT_SMS_UPDATE";
    public static final String ACTION_RECEIVED_SMS_UPDATE = "com.android.littleutilities.easystats.ACTION_RECEIVED_SMS_UPDATE";
    public static final String ACTION_SENT_MMS_UPDATE = "com.android.littleutilities.easystats.ACTION_SENT_MMS_UPDATE";
    public static final String ACTION_RECEIVED_MMS_UPDATE = "com.android.littleutilities.easystats.ACTION_RECEIVED_MMS_UPDATE";
    public static final String ACTION_INCOMING_CALLS_UPDATE = "com.android.littleutilities.easystats.ACTION_INCOMING_CALLS_UPDATE";
    public static final String ACTION_OUTGOING_CALLS_UPDATE = "com.android.littleutilities.easystats.ACTION_OUTGOING_CALLS_UPDATE";
    public static final String ACTION_IN_DATA_UPDATE = "com.android.littleutilities.easystats.ACTION_IN_DATA_UPDATE";
    public static final String ACTION_OUT_DATA_UPDATE = "com.android.littleutilities.easystats.ACTION_OUT_DATA_UPDATE";
    public static final String ACTION_DATA_UPDATE = "com.android.littleutilities.easystats.ACTION_DATA_UPDATE";
    public static final String ACTION_AMALGAMATED_UPDATE = "com.android.littleutilities.easystats.ACTION_AMALGAMA_UPDATE";

    private static final String TAG = "StatisticDataService";
//    private ConnectivityManager mConnMgr = null;
    private TelephonyManager mTelmgr = null;
    private FileReader mNetFile = null;
    private BufferedReader mInBuf = null;

    final private String DEV_FILE = "/proc/self/net/dev";
    //what I've seen in howto for WiFi
    final private String WIFI_DEV = "twlan0";
    //what I've seen by my own eyes in Korea for WiFi
    final private String ETH_DEV = "eth0";
    //what works for 3G data in Korea
    final private String CELL_DEV = "rmnet0";

    //what works for 3G in Verizon network
    final private String CELL_PPP = "ppp0";
    
    final private int MESSAGE_ID_CHECK_NETSTAT = 0;
    final private int MESSAGE_ID_UPDATE_DATA = 1;
    final private int MESSAGE_ID_UPDATE_CALLS = 2;
    final private int MESSAGE_ID_UPDATE_MESSAGES = 3;
    
    final private int NETSTAT_CHECK_INTERVAL = 10000; //every second
    
    //These variables don't count real number of sent messages.
    //This is only to keep track of updates happening in SMS provider.
    private int mSmsCounter = 0; // this one holds current amount of sent SMS messages saved in SMS provider
    private int mLastSmsId = -1; // this one holds an ID of the latest sent SMS message saved in SMS provider

    private int mMmsCounter = 0; // this one holds current amount of sent MMS messages saved in SMSMMS provider
    private int mLastMmsId = -1; // this one holds an ID of the latest sent MMS message saved in SMSMMS provider

    private int mMmsInCounter = 0; // this one holds current amount of received MMS messages saved in SMSMMS provider
    private int mLastInMmsId = -1; // this one holds an ID of the latest received MMS message saved in SMSMMS provider


    private int mIntInCallCounter = 0; //holds current amount of received calls in CallLog database
    private int mLastInCallId = -1; //an ID of the latest record for incoming call in CallLog DB
    
    private int mIntOutCallCounter = 0; //holds current amount of placed calls in CallLog database
    private int mLastOutCallId = -1; //an ID of the latest record for outgoing call in CallLog DB
    
    // This is the counter of the sent messages counted so far
    // This one is our monitored counter
    private int mSentSmsCounter = 0;
    private int mSentMmsCounter = 0;
    
    // This is the counter for received SMS messages
    private int mReceivedSmsCounter = 0;
    private int mReceivedMmsCounter = 0;
    
    //Counters for incoming/outgoing calls
    private int mOutCallCounter = 0;
    private int mInCallCounter = 0;
    private int mOutCallDuration = 0;
    private int mInCallDuration = 0;
    
    private long mLastReceivedData = 0;
    private long mLastSentData = 0;
    private long mTotalReceivedData = 0;
    private long mTotalSentData = 0;
    

    private ContentObserver mSmsObserver;
    private ContentObserver mMmsObserver;
    private ContentObserver mCallsObserver;
//    private FileObserver mNetIfObserver;
    
    //DATABASE SUPPORT
    private StatisticDatabaseOpenHelper mDbOpener = null;

    private Context mContext = null;
    private Context getContext()
    {
        return mContext;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	switch (msg.what) {
        		case MESSAGE_ID_CHECK_NETSTAT: {
        			Log.e(TAG, "msg - check stats");
        			getNetworkIfStatistics(false);
        			mHandler.sendEmptyMessageDelayed(MESSAGE_ID_CHECK_NETSTAT, NETSTAT_CHECK_INTERVAL);
        		} break;
        		case MESSAGE_ID_UPDATE_DATA: {
        		    getNetworkIfStatistics(false);
        		} break;
        		
        		case MESSAGE_ID_UPDATE_CALLS: {
                    Bundle bundle = msg.getData();
                    Log.e(TAG, "start processing internal calls update...");
                    if (bundle != null) {
                        updateDbCallsTable(bundle.getString("number"), bundle.getInt("duration"), bundle.getInt("direction"));
                    }
        		} break;
        		
        		case MESSAGE_ID_UPDATE_MESSAGES: {
        		    Bundle bundle = msg.getData();
        		    Log.e(TAG, "start processing internal message update...");
        		    if (bundle != null) {
        		        if (bundle.getInt("type") == DataProvider.SMS) {
        		            if (bundle.getInt("direction") == DataProvider.INCOMING) {
        		                handleIncomingSms(bundle.getString("number"));
        		            } else {
        		                updateDbMessagesTable(bundle.getString("number"), DataProvider.SMS, DataProvider.OUTGOING, bundle.getInt("count"));
        		            }
        		        } else {
                            updateDbMessagesTable(bundle.getString("number"), DataProvider.MMS, bundle.getInt("direction"), bundle.getInt("count"));
        		        }
        		    }
        		} break;

        		default:
        			break;
        	}
        }
    };
    
    private BroadcastReceiver mInterceptor = new BroadcastReceiver()
    {
        
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if ( (intent.getAction().equals("android.intent.action.DATA_SMS_RECEIVED")) ||
                 (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) )  {
                //SMS received
                Bundle bundle = intent.getExtras();
                SmsMessage message = null;

                if (bundle != null) {
                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    message = SmsMessage.createFromPdu((byte[]) pdusObj[0]);
                }

                if (message != null) {
                    scheduleMessageStatisticUpdate(message.getDisplayOriginatingAddress(), DataProvider.INCOMING, DataProvider.SMS, 1);
                } else {
                    scheduleMessageStatisticUpdate("UNKNOWN", DataProvider.INCOMING, DataProvider.SMS, 1);
                }
            } else if ( (intent.getAction().equals(Intent.ACTION_SHUTDOWN)) ||
                        (intent.getAction().equals(Intent.ACTION_REBOOT)) ) {
                Log.e(TAG, "Need to save all the pending data and close DataBases");
                handleDownEvent();
            }
        }
    };

    private PhoneStateListener mPhoneCallListener = new PhoneStateListener()
    {        
        public void onDataActivity(int direction)
        {
            if (direction != TelephonyManager.DATA_ACTIVITY_NONE &&
                direction != TelephonyManager.DATA_ACTIVITY_DORMANT)
            	scheduleDataStatisticUpdate();
        }
        
        public void onDataConnectionStateChanged(int state, int networkType)
        {
            Log.e(TAG, "onDataConnectionStateChanged state: " + state + " type: " + networkType);
            if (state == TelephonyManager.DATA_DISCONNECTED || state == TelephonyManager.DATA_SUSPENDED) {
            	//Let's check the WiFi status
            	Log.e(TAG, "3g dropped");
            	// WiFi monitoring is not yet implemented (don't see why one will need it)
            	//mHandler.sendEmptyMessageDelayed(MESSAGE_ID_CHECK_NETSTAT, NETSTAT_CHECK_INTERVAL);
            } else {
            	Log.e(TAG, "3g is back");
            	mHandler.removeMessages(MESSAGE_ID_CHECK_NETSTAT);
            }
        }
    };

    private void scheduleDataStatisticUpdate()
    {
        if (mHandler != null) {
            //We don't need to process all the data updates, the last one is enough
            mHandler.removeMessages(MESSAGE_ID_UPDATE_DATA);
            mHandler.sendEmptyMessage(MESSAGE_ID_UPDATE_DATA);
        }
    }

    private void scheduleMessageStatisticUpdate(String number, int direction, int type, int count)
    {
        if (mHandler != null) {
            Message msg = mHandler.obtainMessage(MESSAGE_ID_UPDATE_MESSAGES);
            Bundle data = new Bundle();

            data.putString("number", number);
            data.putInt("direction", direction);
            data.putInt("type", type);
            data.putInt("count", count);

            msg.setData(data);
            mHandler.sendMessage(msg);
        }
    }

    private void scheduleCallsStatisticUpdate(String number, int direction, int duration)
    {
        if (mHandler != null) {           
            Message msg = mHandler.obtainMessage(MESSAGE_ID_UPDATE_CALLS);
            Bundle data = new Bundle();

            data.putString("number", number);
            data.putInt("direction", direction);
            data.putInt("duration", duration);

            msg.setData(data);
            mHandler.sendMessage(msg);
        }
    }
    
    private void getNetworkIfStatistics(boolean init)
    {
        try {
            mNetFile = new FileReader(DEV_FILE);
        } catch (Exception e) {
            Log.e(TAG, "Could not read " + DEV_FILE);
            e.printStackTrace();
            return;
        }

        mInBuf = new BufferedReader(mNetFile, 500);
        String line;
        String[] segs;
        try {
            while ((line = mInBuf.readLine()) != null) {
            	line = line.trim();

                if (line.startsWith(CELL_DEV) || line.startsWith(CELL_PPP)) {
                    segs = line.trim().split("[: ]+");

                    if (!init) {
                    	mTotalReceivedData += Integer.decode(segs[1]) - mLastReceivedData;
                    	mTotalSentData += Integer.decode(segs[9]) - mLastSentData;

                    	updateDbDataTable((int)(Integer.decode(segs[1]) - mLastReceivedData), 
                    			(int)(Integer.decode(segs[9]) - mLastSentData), DataProvider.DATA_3G);
                    }

                    mLastReceivedData = Integer.decode(segs[1]);
                    mLastSentData = Integer.decode(segs[9]);

                    if (!init) {
                    	//Notify WidgetProvider about UI update
                    	notifyInOutData();
                    }
                } else if (line.startsWith(WIFI_DEV) || line.startsWith(ETH_DEV)) {
                    //WiFi is not supported, because I don't think anyone cares about WiFi traffic
                }
            }
        } catch (Exception e) {
                Log.e(TAG, e.toString());
        }
        
    }

    private void handleSentSms()
    {
        // We receive notifications from SMS provider any time any change happens in sms DB
        // What we need is to determine whether it is just a new set sms
        // In this regard we check total count of sent messages
        // If it reduced, then user deleted some messages
        // If it increased, then user has sent a message (our case!)
        
        ContentResolver cr = getContext().getContentResolver();
        Cursor cur = null;
        String projection[] = {"COUNT(_id)", "MAX(_id)"}; // we need only total count of sent sms and the highest _id value
        String values[] = {"2"}; // this is hard code for type of sent sms
        int counter = 0;
        
        cur = cr.query(Uri.parse("content://sms"), projection, "type=?",values, null);
        //cur = cr.query(Uri.parse("content://sms"), null, "type=?",values, null);
        if (cur != null) {
            if ((cur.getCount() > 0) && cur.moveToFirst()) {

                if (mSmsCounter < cur.getInt(0)) {
                    Log.e(TAG, "Got a new SENT SMS for sure");

                    //Update local counter
                    mSentSmsCounter += (cur.getInt(0) - mSmsCounter);
                    counter = cur.getInt(0) - mSmsCounter;
                }

                mSmsCounter = cur.getInt(0);
                mLastSmsId = cur.getInt(1);
                Log.e(TAG, "total sent sms: " + mSmsCounter + ", latest _id: " + mLastSmsId);
            }
            else {
                Log.e(TAG, "No records found");
            }
            cur.close();
            
            if (counter != 0) {
                String[] proj = {"address"};
                String number = "UNKNOWN";

                cur = cr.query(Uri.parse("content://sms"), proj, "_id= "+Integer.toString(mLastSmsId),null, null);

                if (cur != null) {
                    if (cur.getCount() > 0 && cur.moveToFirst())
                        number = cur.getString(0);
                    
                    cur.close();
                }
                //Update internal DB with the information
                Log.e(TAG, "SENT " + counter +" SMS to " + number);
                scheduleMessageStatisticUpdate(number, DataProvider.OUTGOING, DataProvider.SMS, counter);
    
                //Notify WidgetProvider about UI update
                notifyOutgoingSms();
            }
        }
    }
    
    
    // It is more convenient to handle received MMS in this function as well
    // Every time when the MMS is sent, MMS provider gets updated
    // It eventually triggers the function
    // We are searching for the new ID(row) and check its message box to determine if it is in or out
    private void handleSentMms()
    {
        ContentResolver cr = getContext().getContentResolver();
        Cursor cur = null;
        String projection[] = {"COUNT(_id)", "MAX(_id)"}; // we need only total count of sent sms and the highest _id value
        String values_out[] = {"2"}; // this is hard code for type of sent sms
        String values_in[] = {"1"}; // received MMS
        int counter = 0;

/////// =================== HANDLE OUTGOING MMS ============================================
        cur = cr.query(Uri.parse("content://mms"), projection, "msg_box=?",values_out, null);
        //cur = cr.query(Uri.parse("content://mms"), null, "msg_box=?",values, null);

        if (cur != null) {
            if ((cur.getCount() > 0) && cur.moveToFirst()) {
                if (mMmsCounter < cur.getInt(0)) {
                    Log.e(TAG, "Got a new SENT MMS for sure");

                    //Update local counter
                    mSentMmsCounter += counter = (cur.getInt(0) - mMmsCounter);
                }

                mMmsCounter = cur.getInt(0);
                mLastMmsId = cur.getInt(1);
                Log.e(TAG, "total sent mms: " + mMmsCounter + ", latest mms _id: " + mLastMmsId);
            }
            else {
                Log.e(TAG, "No records for mms found");
            }

            cur.close();
            
            if (counter > 0) {
                String[] proj = {"thread_id"};
                int thread_id = -1;
                cur = cr.query(Uri.parse("content://mms"), proj, "_id= "+ Integer.toString(mLastMmsId),null, null);

                if (cur != null) {
                    if (cur.getCount() > 0 && cur.moveToFirst()) {
                        thread_id = cur.getInt(0);
                    }

                    cur.close();
                }

                //need to get number via thread_id matching in SMS DB
                String sms_proj[] = {"address"};
                String sms_values[] = {Integer.toString(thread_id)};
                String number;
    
                Cursor sms_cur = cr.query(Uri.parse("content://sms"), sms_proj, "thread_id=?",sms_values, null);
                if (sms_cur != null && sms_cur.moveToFirst()) {
                    number = sms_cur.getString(0);
                } else {
                    number = "UNKNOWN";
                }
    
                if (sms_cur != null)
                    sms_cur.close();
    
                Log.e(TAG, "SENT " +counter+ " MMS to " + number);
                //Update internal DB with the information
                scheduleMessageStatisticUpdate(number, DataProvider.OUTGOING, DataProvider.MMS, counter);
    
                //Notify WidgetProvider about UI update
                notifyOutgoingMms();   
            }
        }
////// ================================= END HANDLE OUTGOING MMS =========================
        
////// =============================== HANDLE INCOMING MMS ===============================
        cur = cr.query(Uri.parse("content://mms"), projection, "msg_box=?",values_in, null);

        counter = 0;

        if (cur != null) {
            if ((cur.getCount() > 0) && cur.moveToFirst()) {
                if (mMmsInCounter < cur.getInt(0)) {
                    Log.e(TAG, "Got a new RECEIVED MMS for sure");

                    //Update local counter
                    mReceivedMmsCounter += counter = (cur.getInt(0) - mMmsInCounter);
                }

                mMmsInCounter = cur.getInt(0);
                mLastInMmsId = cur.getInt(1);
                Log.e(TAG, "total received mms: " + mMmsInCounter + ", latest mms _id: " + mLastInMmsId);
            }
            else {
                Log.e(TAG, "No records for mms found");
            }

            cur.close();
            
            if (counter > 0) {
                String[] proj = {"thread_id"};
                int thread_id = -1;
                cur = cr.query(Uri.parse("content://mms"), proj, "_id= "+ Integer.toString(mLastInMmsId),null, null);

                if (cur != null) {
                    if (cur.getCount() > 0 && cur.moveToFirst()) {
                        thread_id = cur.getInt(0);
                        Log.e(TAG, "found thread Id = " + thread_id);
                    }

                    cur.close();
                }

                //need to get number via thread_id matching in SMS DB
                String sms_proj[] = {"address"};
                String sms_values[] = {Integer.toString(thread_id)};
                String number;
    
                Cursor sms_cur = cr.query(Uri.parse("content://sms"), sms_proj, "thread_id=?",sms_values, null);
                if (sms_cur != null && sms_cur.moveToFirst()) {
                    number = sms_cur.getString(0);
                    Log.e(TAG, "found the number - " + number);
                } else {
                    number = "UNKNOWN";
                    Log.e(TAG, "not found the number");
                }
    
                if (sms_cur != null)
                    sms_cur.close();
    
                Log.e(TAG, "RECEIVED " +counter+ " MMS to " + number);
                //Update internal DB with the information
                scheduleMessageStatisticUpdate(number, DataProvider.INCOMING, DataProvider.MMS, counter);
    
                //Notify WidgetProvider about UI update
                notifyIncomingMms();   
            }
        }
///////// ============================================= END INCOMING HANDLING ===============================
    }
    
    private void handleIncomingSms(String number)
    {
        //Update local counter
        ++mReceivedSmsCounter;

        updateDbMessagesTable(number, DataProvider.SMS, DataProvider.INCOMING, 1);

        //Notify WidgetProvider about UI update
        notifyIncomingSms();
    }
    
    private void handleIncomingMms(String number)
    {
        //Update local counter
        ++mReceivedMmsCounter;

        updateDbMessagesTable(number, DataProvider.MMS, DataProvider.INCOMING, 1);

        //Notify WidgetProvider about UI update
        notifyIncomingMms();
    }

    private void notifyAllData()
    {
        Intent intent = new Intent(ACTION_AMALGAMATED_UPDATE);

        intent.putExtra("cRecvMms", mReceivedMmsCounter);
        intent.putExtra("cRecvSms", mReceivedSmsCounter);
        intent.putExtra("cSentMms", mSentMmsCounter);
        intent.putExtra("cSentSms", mSentSmsCounter);
        intent.putExtra("cSentCall", mOutCallCounter);
        intent.putExtra("dSentCall", mOutCallDuration);
        intent.putExtra("cRecvCall", mInCallCounter);
        intent.putExtra("dRecvCall", mInCallDuration);
        intent.putExtra("cRecvData", mTotalReceivedData);
        intent.putExtra("cSentData", mTotalSentData);

        getContext().sendBroadcast(intent);
    }

    private void notifyIncomingMms()
    {
        /*
        //Notify WidgetProvider about UI update
        Intent intent = new Intent(ACTION_RECEIVED_MMS_UPDATE);
        Log.e(TAG, "received  Mms "+ mReceivedMmsCounter);
        intent.putExtra("count", mReceivedMmsCounter);
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }
    
    private void notifyIncomingSms()
    {
        /*
        Intent intent = new Intent(ACTION_RECEIVED_SMS_UPDATE);
        Log.e(TAG, "received  Sms "+ mReceivedSmsCounter);
        intent.putExtra("count", mReceivedSmsCounter);
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }

    private void notifyOutgoingMms()
    {
        /*
        Intent intent = new Intent(ACTION_SENT_MMS_UPDATE);
        Log.e(TAG, "sent  mms "+ mSentMmsCounter);
        intent.putExtra("count", mSentMmsCounter);
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }

    private void notifyOutgoingSms()
    {
        /*
        Intent intent = new Intent(ACTION_SENT_SMS_UPDATE);
        Log.e(TAG, "sent  Sms "+ mSentSmsCounter);
        intent.putExtra("count", mSentSmsCounter);
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }
    
    private void notifyIncomingCalls()
    {
        /*
        Intent intent = new Intent(ACTION_INCOMING_CALLS_UPDATE);
        intent.putExtra("count", mInCallCounter);
        intent.putExtra("duration", mInCallDuration);
        Log.e(TAG, "Send an update for incoming calls counter " + mInCallCounter + " "+ mInCallDuration );
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }

    private void notifyOutgoingCalls()
    {
        /*
        Intent intent = new Intent(ACTION_OUTGOING_CALLS_UPDATE);
        intent.putExtra("count", mOutCallCounter);
        intent.putExtra("duration", mOutCallDuration);
        Log.e(TAG, "Send an update for outgoing calls counter " + mOutCallCounter + " "+ mOutCallDuration );
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }

    private void notifyInOutData()
    {
        /*
        Intent intent = new Intent(ACTION_DATA_UPDATE);
        intent.putExtra("in_data", mTotalReceivedData);
        intent.putExtra("out_data", mTotalSentData);
        Log.e(TAG, "data "+ mTotalReceivedData + "  " + mTotalSentData);
        getContext().sendBroadcast(intent);
        */
        notifyAllData();
    }
    
    private void handleCalls()
    {
        Log.e(TAG, "Calls table has been updated...");

        ContentResolver cr = getContext().getContentResolver();
        Cursor cur = null;
        String projection[] = {"COUNT(_id) AS id_counter", "MAX(_id) AS id_max"};
        String values_out[] = {Integer.toString(CallLog.Calls.OUTGOING_TYPE)};
        String values_in[] = {Integer.toString(CallLog.Calls.INCOMING_TYPE)};
        int counter = 0;

        cur = cr.query(CallLog.Calls.CONTENT_URI, projection, CallLog.Calls.TYPE+"=?",values_out, null);

        if (cur != null) {
            if ((cur.getCount() > 0) && cur.moveToFirst()) {
                if (mIntOutCallCounter < cur.getInt(0)) {
                    Log.e(TAG, "Got a new outgoing call");

                    //Update local counter
                    counter = (cur.getInt(0) - mIntOutCallCounter);
                    mOutCallCounter += counter;
                }

                mIntOutCallCounter = cur.getInt(0);
                mLastOutCallId = cur.getInt(1);
                Log.e(TAG, "total out calls: " + mIntOutCallCounter + ", latest call _id: " + mLastOutCallId);
            }

            cur.close();
        }

        if (counter > 0) {
            String[] proj = {CallLog.Calls.NUMBER,CallLog.Calls.DURATION };

            cur = cr.query(CallLog.Calls.CONTENT_URI, proj, "_id= "+Integer.toString(mLastOutCallId),null, null);

            if (cur != null) {
                if (cur.getCount() > 0 && cur.moveToFirst()) {
                    Log.e(TAG, "placed a phone call to " + cur.getString(0) + " lasted for " + Integer.toString(cur.getInt(1)));

                    //Update internal DB with the information                    
                    scheduleCallsStatisticUpdate(cur.getString(0), DataProvider.OUTGOING, cur.getInt(1));
                    mOutCallDuration += cur.getInt(1);

                    //Notify WidgetProvider about UI update
                    notifyOutgoingCalls();
                }

                cur.close();
            }
        }

        counter = 0;
        cur = cr.query(CallLog.Calls.CONTENT_URI, projection, CallLog.Calls.TYPE+"=?",values_in, null);

        if (cur != null) {
            if ((cur.getCount() > 0) && cur.moveToFirst()) {
                if (mIntInCallCounter < cur.getInt(0)) {
                    Log.e(TAG, "Got a new incoming call");

                    //Update local counter
                    counter = (cur.getInt(0) - mIntInCallCounter);
                    mInCallCounter += counter;
                }

                mIntInCallCounter = cur.getInt(0);
                mLastInCallId = cur.getInt(1);
                Log.e(TAG, "total in calls: " + mIntInCallCounter + ", latest call _id: " + mLastInCallId);
            }

            cur.close();
        }

        if (counter > 0) {
            String[] proj = {CallLog.Calls.NUMBER,CallLog.Calls.DURATION };

            cur = cr.query(CallLog.Calls.CONTENT_URI, proj, "_id= "+Integer.toString(mLastInCallId),null, null);

            if (cur != null) {
                if (cur.getCount() > 0 && cur.moveToFirst()) {
                    Log.e(TAG, "received a phone call from " + cur.getString(0) + " lasted for " + Integer.toString(cur.getInt(1)));

                    //Update internal DB with the information
                    scheduleCallsStatisticUpdate(cur.getString(0), DataProvider.INCOMING, cur.getInt(1));
                    mInCallDuration += cur.getInt(1);
                    //Notify WidgetProvider about UI update
                    notifyIncomingCalls();
                }
                cur.close();
            }
        }
    }


	private void updateDbDataTable(int inDataSize,  int outDataSize, int type) {
		//current date
		long date = Util.getCurrentDateCode();
		
		String projection[] = {DataProvider.IN_SIZE, DataProvider.OUT_SIZE };
		
		String[] selectionArgs = {Long.toString(date), Integer.toString(type)};
		
		Cursor cur = null;

		try {

		    ContentValues values = new ContentValues(4);
            
            values.put(DataProvider.DATE, date);
            values.put(DataProvider.TYPE, type);

		    cur = getDB().query(DataProvider.DATA, projection, DataProvider.DATE+" = ? AND " + DataProvider.TYPE + " = ?", 
		            selectionArgs, null, null, null);
		    
		    if ((cur != null) && (cur.moveToFirst())) {
		        //There is already one record in DB, so we need to modify it
		        for (int i = 0; i < cur.getCount(); ++i) {
		            
		            values.put(DataProvider.IN_SIZE, inDataSize + cur.getInt(0));
		            values.put(DataProvider.OUT_SIZE, outDataSize + cur.getInt(1));
		            cur.moveToNext();
		        }
		        Log.e(TAG, "Updating DB with data stats ...");
                getDB().update(DataProvider.DATA, values, DataProvider.DATE+" = ? AND " + DataProvider.TYPE + " = ?", selectionArgs);		        
		    } else {
		        //there is no any record in DB, so let's insert

		        if (inDataSize != 0) {
		            values.put(DataProvider.IN_SIZE, inDataSize);
		        } else {
		            values.put(DataProvider.IN_SIZE, 0);
		        }

		        if (outDataSize != 0) {
		            values.put(DataProvider.OUT_SIZE, outDataSize);
		        } else {
		            values.put(DataProvider.OUT_SIZE, 0);
		        }

	            Log.e(TAG, "Values to insert: "+ values.toString() + " || "+ values);
	            getDB().insertWithOnConflict(DataProvider.DATA, null, values, SQLiteDatabase.CONFLICT_REPLACE);
		    }

		    if (cur != null)
                cur.close();

		} catch (Exception ex) {
		    
            if (cur != null)
                cur.close();

		    ex.printStackTrace();
		}
	}

    private void updateDbCallsTable(String number, int duration, int direction) {
        
        //current date
        long date = Util.getCurrentDateCode();
        
        String projection[] = {DataProvider.TOTAL_TIME, DataProvider.COUNT };
        
        String[] selectionArgs = {Long.toString(date), number, Integer.toString(direction)};
        
        Cursor cur = null;

        try {

            ContentValues values = new ContentValues(5);
            
            values.put(DataProvider.DATE, date);
            values.put(DataProvider.DIRECTION, direction);
            values.put(DataProvider.NUMBER, number);

            cur = getDB().query(DataProvider.CALLS, projection, DataProvider.DATE+" = ? AND " + DataProvider.NUMBER + " = ? AND " + DataProvider.DIRECTION + " = ?", 
                    selectionArgs, null, null, null);
            
            if ((cur != null) && (cur.moveToFirst())) {
                //There is already one record in DB, so we need to modify it
                for (int i = 0; i < cur.getCount(); ++i) {
                    
                    values.put(DataProvider.TOTAL_TIME, duration + cur.getInt(0));
                    values.put(DataProvider.COUNT, cur.getInt(1) + 1);

                    cur.moveToNext();
                }
                Log.e(TAG, "update val "+ values.toString() + " || "+ values);
                getDB().update(DataProvider.CALLS,
                        values, 
                        DataProvider.DATE+" = ? AND " + DataProvider.NUMBER + " = ? AND " + DataProvider.DIRECTION + " = ?", 
                        selectionArgs);                

            } else {
                //there is no any record in DB, so let's insert
                values.put(DataProvider.TOTAL_TIME, duration);
                values.put(DataProvider.COUNT, 1);
                Log.e(TAG, "insert val "+ values.toString() + " || "+ values);
                getDB().insertWithOnConflict(DataProvider.CALLS, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }

            if (cur != null)
                cur.close();

        } catch (Exception ex) {
            ex.printStackTrace();

            if (cur != null)
                cur.close();
        }
    }

    //==================================================================================
    // Function: updateDbMessagesTable
    // Description: This function stores messaging statistics in internal DB
    // 
    // number - string representing the phone number or 'UNKNOWN'
    // type - represents a type of a message (SMS/MMS)
    // direction - indicates whether the message is incoming or outgoing
    // count - number of messages of this type+direction+number to be added

    private void updateDbMessagesTable(String number, int type, int direction, int count) {

        //current date
        long date = Util.getCurrentDateCode();
        
        String projection[] = {DataProvider.COUNT };
        
        String[] selectionArgs = {Long.toString(date), number, Integer.toString(direction), Integer.toString(type)};
        
        Cursor cur = null;

        try {

            ContentValues values = new ContentValues(5);
            
            values.put(DataProvider.DATE, date);
            values.put(DataProvider.DIRECTION, direction);
            values.put(DataProvider.NUMBER, number);
            values.put(DataProvider.TYPE, type);

            cur = getDB().query(DataProvider.MESSAGES, projection, 
                    DataProvider.DATE+" = ? AND " + DataProvider.NUMBER + " = ? AND " + DataProvider.DIRECTION + " = ? AND " + DataProvider.TYPE + " = ?", 
                    selectionArgs, null, null, null);
            
            if ((cur != null) && (cur.moveToFirst())) {
                //There is already one record in DB, so we need to modify it
                for (int i = 0; i < cur.getCount(); ++i) {
                    values.put(DataProvider.COUNT, cur.getInt(0) + count);
                    cur.moveToNext();
                }

                Log.e(TAG, "Updating DB with messages stats ..." + values.toString());
                getDB().update(DataProvider.MESSAGES,
                        values, 
                        DataProvider.DATE+" = ? AND " + DataProvider.NUMBER + " = ? AND " + DataProvider.DIRECTION + " = ? AND " + DataProvider.TYPE + " = ?", 
                        selectionArgs);                
            } else {
                //there is no any record in DB, so let's insert
                values.put(DataProvider.COUNT, count);
                Log.e(TAG, "Values to insert: "+ values.toString() + " || "+ values);
                getDB().insertWithOnConflict(DataProvider.MESSAGES, null, values, SQLiteDatabase.CONFLICT_REPLACE);
            }

            if (cur != null)
                cur.close();

        } catch (Exception ex) {
            if (cur != null)
                cur.close();

            ex.printStackTrace();
        }
    }

    private void handleDownEvent()
    {
        Log.e(TAG, "Shutting down event processing...");
        try {
        	getContext().unregisterReceiver(mInterceptor);
        } catch (Exception e) {
            //do nothing
            e.printStackTrace();
        }
        
        ContentResolver cr = getContext().getContentResolver();
        Log.e(TAG, "Shutting down content observers...");
        try {
            cr.unregisterContentObserver(mSmsObserver);
        } catch (Exception e){
            //do nothing
            e.printStackTrace();
        }
        try {
            cr.unregisterContentObserver(mMmsObserver);
        }catch (Exception e) {
            //do nothing
            e.printStackTrace();
        }
        
        try {
            cr.unregisterContentObserver(mCallsObserver);
        } catch(Exception e) {
            //do nothing
            e.printStackTrace();
        }
        
        Log.e(TAG, "Stop listening for phone calls");
        try {
            if (mTelmgr != null) {
                mTelmgr.listen(mPhoneCallListener, PhoneStateListener.LISTEN_NONE);
                mTelmgr = null;
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "Close file readers...");
        try {
        	if (mInBuf != null) {
        		mInBuf.close();
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (mNetFile != null) {
                mNetFile.close();
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "Close database");

        try {
            if (mDbOpener != null)
                mDbOpener.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    protected SQLiteDatabase getDB()
    {
        synchronized (this) {
            if (mDbOpener == null) {
                mDbOpener = new StatisticDatabaseOpenHelper(getApplicationContext());
            }

            Log.e(TAG, "Opening the statistic Database...");
            return mDbOpener.getWritableDatabase();
        }
    }

    protected void resetDB(Message msg)
    {
        synchronized(this) {
            //mDbOpener.rebuildDatabase(mDbOpener.getWritableDatabase());
            if (msg.getData().getBoolean(DataProvider.MESSAGES, false) == true) {
                mDbOpener.doDropTable(DataProvider.MESSAGES);
                mDbOpener.getWritableDatabase().execSQL(StatisticDatabaseOpenHelper.MESSAGES_TABLE_SCHEME);
            } else if (msg.getData().getBoolean(DataProvider.CALLS, false) == true) {
                mDbOpener.doDropTable(DataProvider.CALLS);
                mDbOpener.getWritableDatabase().execSQL(StatisticDatabaseOpenHelper.CALLS_TABLE_SCHEME);
            } else if (msg.getData().getBoolean(DataProvider.DATA, false) == true) {
                mDbOpener.doDropTable(DataProvider.DATA);
                mDbOpener.getWritableDatabase().execSQL(StatisticDatabaseOpenHelper.DATA_TABLE_SCHEME);
            }
        }
    }

    protected void closeDB()
    {
        synchronized (this) {
            mDbOpener.close();
            mDbOpener = null;
        }
    }
    
    private void initInternalState()
    {
        Cursor cur = null;
        
     // ========= 1. Let's read the SMS/MMS statistics from the Statistics Database

                 String columns[] = {"SUM(" + DataProvider.COUNT +"), " + DataProvider.TYPE + ", " + DataProvider.DIRECTION };
                 //Read sums of sent and received sms and mms
                 //SELECT SUM(count), type, direction FROM messages GROUP BY type, direction;
                 cur = getDB().query(DataProvider.MESSAGES, columns, null, null, DataProvider.TYPE + ", " + DataProvider.DIRECTION, null, null);
                 
                 if (cur != null) {
                     if (cur.getCount() > 0 && cur.moveToFirst()) {
                         for (int i = 0; i < cur.getCount(); ++i) {

                             if (cur.getInt(1) == DataProvider.SMS) {
                                 if (cur.getInt(2) == DataProvider.INCOMING) {
                                     mReceivedSmsCounter = cur.getInt(0);
                                     Log.e(TAG, "received SMS counter read from DB:" + mReceivedSmsCounter);
                                     notifyIncomingSms();
                                 } else {
                                     mSentSmsCounter = cur.getInt(0);
                                     Log.e(TAG, "sent SMS counter read from DB:" + mSentSmsCounter);
                                     notifyOutgoingSms();
                                 }
                             } else {
                                 if (cur.getInt(2) == DataProvider.INCOMING) {
                                     mReceivedMmsCounter = cur.getInt(0);
                                     Log.e(TAG, "received MMS counter read from DB:" + mReceivedMmsCounter);
                                     notifyIncomingMms();
                                 } else {
                                     mSentMmsCounter = cur.getInt(0);
                                     Log.e(TAG, "sent MMS counter read from DB:" + mSentMmsCounter);
                                     notifyOutgoingMms();
                                 }
                             }
                             cur.moveToNext();
                         }
                     } else {
                         mReceivedSmsCounter = 0;
                         mSentSmsCounter = 0;
                         mReceivedMmsCounter = 0;
                         mSentMmsCounter = 0;
                     }
                     cur.close();
                 }
     // =======================================================================
     // ========= 2. READ the CALLS statistics from the Statistics Database

                 String columns1[] = {"SUM(" + DataProvider.COUNT +"), SUM(" + DataProvider.TOTAL_TIME + ")," + DataProvider.DIRECTION };
                 //Read sums of sent and received phone calls
                 //SELECT SUM(count), SUM(total_time), direction from calls GROUP BY direction;
                 cur = getDB().query(DataProvider.CALLS, columns1, null, null, DataProvider.DIRECTION, null, null);
                 
                 if (cur != null) {
                     if (cur.getCount() > 0 && cur.moveToFirst()) {
                         for (int i = 0; i < cur.getCount(); ++i) {

                             if (cur.getInt(2) == DataProvider.INCOMING) {
                                 mInCallCounter = cur.getInt(0);
                                 mInCallDuration = cur.getInt(1);
                                 Log.e(TAG, "received calls duration: "+ mInCallDuration+" and count: "+ mInCallCounter);
                                 notifyIncomingCalls();
                             } else {
                                 mOutCallCounter = cur.getInt(0);
                                 mOutCallDuration = cur.getInt(1);
                                 Log.e(TAG, "placed calls duration: "+ mOutCallDuration+" and count: "+ mOutCallCounter);
                                 notifyOutgoingCalls();
                             }
                             cur.moveToNext();
                         }
                     } else {
                         mInCallCounter = mInCallDuration = mOutCallCounter = mOutCallDuration = 0;
                     }
                     cur.close();
                 }
     // =======================================================================
     // ========= 2. READ the DATA statistics from the Statistics Database

                 String columns2[] = {"SUM(" + DataProvider.IN_SIZE +"), SUM(" + DataProvider.OUT_SIZE + "), " + DataProvider.TYPE};
                 //Read traffic information from the DB
                 //SELECT SUM(in_size),SUM(out_size), type from data GROUP BY type;
                 cur = getDB().query(DataProvider.DATA, columns2, null, null, DataProvider.TYPE, null, null);
                 
                 if (cur != null) {
                     if (cur.getCount() > 0 && cur.moveToFirst()) {
                         for (int i = 0; i < cur.getCount(); ++i) {

                             if (cur.getInt(2) == DataProvider.DATA_3G) {
                                 mTotalReceivedData = cur.getInt(0);
                                 mTotalSentData = cur.getInt(1);
                                 Log.e(TAG, "Sent/Received data from DB: IN - " + mTotalReceivedData+" OUT - " + mTotalSentData);
                             } else {//WiFi is not yet calculated
                             }
                             cur.moveToNext();
                         }

                         notifyInOutData();
                     } else {
                         mTotalReceivedData = mTotalSentData = 0;
                     }

                     cur.close();
                 }
     // =======================================================================

     // ==== Read SMS/MMS information from the sms/mms databases(providers)
     // ==== we need this information to properly calculate our statistics

             ContentResolver cr = getContext().getContentResolver();
             String projection[] = {"COUNT(_id)", "MAX(_id)"}; // we need only total count of sent sms and the highest _id value
             String values[] = {"2"}; // this is hard code for type of sent sms

             cur = cr.query(Uri.parse("content://sms"), projection, "type=?",values, null);

             if (cur != null) {
                 if ((cur.getCount() > 0) && cur.moveToFirst()) {
                     mSmsCounter = cur.getInt(0);
                     mLastSmsId = cur.getInt(1);
                     Log.e(TAG, " From other DBs' total sent sms: " + mSmsCounter + ", latest _id: " + mLastSmsId);
                 }
                 else {
                     Log.e(TAG, "No records found");
                 }
                 cur.close();
             }
             
             cur = cr.query(Uri.parse("content://mms"), projection, "msg_box=?", values, null);

             if (cur != null) {
                 if ((cur.getCount() > 0) && cur.moveToFirst()) {
                     mMmsCounter = cur.getInt(0);
                     mLastMmsId = cur.getInt(1);
                     Log.e(TAG, "From other DBs' total sent mms: " + mMmsCounter + ", latest mms _id: " + mLastMmsId);
                 }
                 else {
                     Log.e(TAG, "No records for mms found");
                 }
                 cur.close();
             }

             {

             String[] values_in = {"1"};
             cur = cr.query(Uri.parse("content://mms"), projection, "msg_box=?", values_in, null);

             if (cur != null) {
                 if ((cur.getCount() > 0) && cur.moveToFirst()) {
                     mMmsInCounter = cur.getInt(0);
                     mLastInMmsId = cur.getInt(1);
                     Log.e(TAG, "From other DBs' total received mms: " + mMmsInCounter + ", latest mms _id: " + mLastInMmsId);
                 }
                 else {
                     Log.e(TAG, "No records for mms found");
                 }
                 cur.close();
             }
             }
             
     // === Read calls registered in the calls provider
             String values_out[] = {Integer.toString(CallLog.Calls.OUTGOING_TYPE)};
             String values_in[] = {Integer.toString(CallLog.Calls.INCOMING_TYPE)};
             String projection1[] = {"COUNT(_id) AS Id_Counter, MAX(_id) AS Id_max"};

             cur = cr.query(CallLog.Calls.CONTENT_URI, projection1, CallLog.Calls.TYPE+"=?",values_out, null);


             if (cur != null) {
                 if ((cur.getCount() > 0) && cur.moveToFirst()) {
                     mIntOutCallCounter = cur.getInt(0);
                     mLastOutCallId = cur.getInt(1);
                     Log.e(TAG, "From other DBs' total placed calls: " + mIntOutCallCounter + ", latest _id: " + mLastOutCallId);
                 }
                 else {
                     Log.e(TAG, "No records found");
                 }
                 cur.close();
             }

             cur = cr.query(CallLog.Calls.CONTENT_URI, projection1, CallLog.Calls.TYPE+"=?",values_in, null);

             if (cur != null) {
                 if ((cur.getCount() > 0) && cur.moveToFirst()) {
                     mIntInCallCounter = cur.getInt(0);
                     mLastInCallId = cur.getInt(1);
                     Log.e(TAG, "From other DBs' total received calls: " + mIntInCallCounter + ", latest _id: " + mLastInCallId);
                 }
                 else {
                     Log.e(TAG, "No records found");
                 }
                 cur.close();
             }

     // === Read the traffic data
             getNetworkIfStatistics(true);
             Log.e(TAG, "Received data from file: "+ mLastReceivedData);
             Log.e(TAG, "Sent data from file: "+ mLastSentData);
     //============================================================        
    }

    private void sendUpdateBroadcast()
    {
        //Let's update the UI part with the data we have so far
        notifyAllData();
    }

    @Override
    public void onCreate()
    {
        Log.e(TAG, "onCreate");
        
        mContext = this.getApplicationContext();
        mDbOpener = new StatisticDatabaseOpenHelper(getContext());

        try {

        ContentResolver cr = getContext().getContentResolver();
        initInternalState();
       
        Log.e(TAG, "Registering for incoming SMS and MMS...");
        //1. receive incoming SMS notifications
        IntentFilter intentSMSReceiver = new IntentFilter();
        intentSMSReceiver.addAction("android.intent.action.DATA_SMS_RECEIVED");
        intentSMSReceiver.addAction("android.provider.Telephony.SMS_RECEIVED");
        getContext().registerReceiver(mInterceptor, intentSMSReceiver);

        Log.e(TAG, "Registering for SHUT_DOWN notification...");
        // For internal use, register shutdown receiver
        // Just to be able to close DB when the phone is turned off
        IntentFilter intentDownReceiver = new IntentFilter();
        intentSMSReceiver.addAction(Intent.ACTION_REBOOT);
        intentSMSReceiver.addAction(Intent.ACTION_SHUTDOWN);
        getContext().registerReceiver(mInterceptor, intentDownReceiver);

        Log.e(TAG, "Registering content observers to monitor sent SMS...");
        //3. Register an observer to monitor sent SMS
        mSmsObserver = new ContentObserver(mHandler)
        {
            public void onChange(boolean selfChange) {
                Log.e(TAG, "update happened in sent SMS");
                handleSentSms();
            }
        };

        cr.registerContentObserver(Uri.parse("content://sms"), true, mSmsObserver);

        Log.e(TAG, "Registering content observers to monitor sent MMS...");
        //4. Register an observer to monitor sent MMS
        mMmsObserver = new ContentObserver(mHandler)
        {
            public void onChange(boolean selfChange) {
                Log.e(TAG, "update happened in sent MMS");
                handleSentMms();
            }
        };
        cr.registerContentObserver(Uri.parse("content://mms-sms/"), true, mMmsObserver);
     
        //5. Register for incoming calls
        //6. Register for outgoing calls
        // There is no way to monitor phone calls in Android
        // So I have to monitor calls database instead =((
        mCallsObserver = new ContentObserver(mHandler)
        {
            public void onChange(boolean selfChange) {
                Log.e(TAG, "update happened in CallLog");
                handleCalls();
            }
        };
        cr.registerContentObserver(CallLog.Calls.CONTENT_URI, true, mCallsObserver);


        Log.e(TAG, "Registering phone state listener to monitor calls and data traffic...");
        
        mTelmgr = (TelephonyManager)getContext().getSystemService(Context.TELEPHONY_SERVICE);
        
        mTelmgr.listen(mPhoneCallListener, PhoneStateListener.LISTEN_CALL_FORWARDING_INDICATOR | 
                PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_DATA_ACTIVITY | 
                PhoneStateListener.LISTEN_DATA_CONNECTION_STATE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void onDestroy()
    {
        handleDownEvent();
        super.onDestroy();
    }

//====================================================================================
//                     --== Service implementation part ==--
//====================================================================================
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();

    public static final int MSG_REGISTER_CLIENT = 1;
    public static final int MSG_UNREGISTER_CLIENT = 2;
    public static final int MSG_CLEAR_DATABASE = 3;
    public static final int MSG_DUMP_DATABASE = 4;
    private static final int MSG_LAST_ID = MSG_DUMP_DATABASE; //keep it pointing to the latest added message 

    //talk back messages
    public static final int MSG_BACK_STATUS = MSG_LAST_ID + 1;

    public static final int STATUS_OK = 0;
    public static final int STATUS_FAIL = 1;
    
    /**
     * Handler of incoming messages from clients.
     */
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    sendUpdateBroadcast();
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_CLEAR_DATABASE:
                    //clear the DB and report back
                    resetDB(msg);

                    try {
                        initInternalState();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    sendUpdateBroadcast();

                    try {
                        msg.replyTo.send(Message.obtain(null,
                                MSG_BACK_STATUS, STATUS_OK, 0));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        mClients.remove(msg.replyTo);
                    }
                    break;

                case MSG_DUMP_DATABASE:
                    //dump the DB and report back
                    InputStream input;

                    closeDB(); //Let's close it first
                    
                    try {
                        input = new FileInputStream("/data/data/com.android.littleutilities.easystats/databases/" + StatisticDatabaseOpenHelper.DATABASE_NAME);

                        File dir = new File("/sdcard/debugdump");
                        dir.mkdir();

                        OutputStream output = new FileOutputStream("/sdcard/debugdump/statistics.db");

                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = input.read(buffer))>0) {
                            output.write(buffer, 0, length);
                        }

                        output.flush();
                        output.close();
                        input.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try {
                        msg.replyTo.send(Message.obtain(null,
                                MSG_BACK_STATUS, STATUS_OK, 0));
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        mClients.remove(msg.replyTo);
                    }

                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    private final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public IBinder onBind(Intent arg0)
    {
        return mMessenger.getBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.e(TAG, "onStartCommand " + startId + ": " + intent);
        super.onStartCommand(intent, flags, startId);
        
        // Widget provider is a child of Receiver thus cannot bind to services =(
        // Therefore, we cannot provide narrow update to every connected client(widget)
        // To overcome binding limitation, all widget instances try to start the service
        // We will send a broadcast for UI update to all listeners when onStart happens
        sendUpdateBroadcast();

        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }
    
//=================================================================================================
//                            --== SERVICE IMPLEMENTATION PART END ==--
//=================================================================================================
}
