package com.android.littleutilities.easystats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public final class ServiceBinder
{
    private static final String TAG = "ServiceBinder";

    private Context mContext = null;
    private static ServiceBinder mInstance = null;

    public static ServiceBinder getInstance(Context context)
    {
        if (mInstance == null) {
            mInstance = new ServiceBinder(context);
        }
        
        return mInstance;
    }

    private ServiceBinder(Context context)
    {
        if (context != null) {
            mContext = context;
        }
    };

     //Interface to the service
    private Messenger mService = null;

     //Handler for messages coming from the service
     private class IncomingHandler extends Handler {
         @Override
         public void handleMessage(Message msg) {
             switch (msg.what) {
                 case StatisticDataProvider.MSG_BACK_STATUS:
                     Log.e(TAG,"got status from service " + msg.arg1);
                     break;
                 default:
                     super.handleMessage(msg);
             }
         }
     };

     //interface for the service to talk back
     private final Messenger mMessenger = new Messenger(new IncomingHandler());

     //service connection helper class
     private ServiceConnection mConnection = new ServiceConnection() {
         public void onServiceConnected(ComponentName className,
                 IBinder service) {

             mService = new Messenger(service);

             // We want to monitor the service for as long as we are
             // connected to it.
             try {
                 Message msg = Message.obtain(null,
                         StatisticDataProvider.MSG_REGISTER_CLIENT);
                 msg.replyTo = mMessenger;
                 mService.send(msg);
             } catch (RemoteException e) {
                 e.printStackTrace();
             }
         }

         public void onServiceDisconnected(ComponentName className) {
             mService = null;
         }
     };
     
     public final void doBindService() {
         mContext.bindService(new Intent(mContext, 
                 StatisticDataProvider.class), mConnection, Context.BIND_AUTO_CREATE);
     }

     public final void doUnbindService() {
         if (mService != null) {
             try {
                 Message msg = Message.obtain(null,
                         StatisticDataProvider.MSG_UNREGISTER_CLIENT);
                 msg.replyTo = mMessenger;
                 mService.send(msg);
             } catch (RemoteException e) {
                 e.printStackTrace();
             }
         }

         // Detach our existing connection.
         mContext.unbindService(mConnection);
     }
     
     public  final void doSendMsg(Message msg)
     {
         if (mService != null) {
             try {
                 if (msg != null) {
                     msg.replyTo = mMessenger;
                     mService.send(msg);
                 }
             } catch (RemoteException e) {
                 e.printStackTrace();
             }
         }
     }
}
