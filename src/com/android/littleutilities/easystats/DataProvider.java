/**
 * 
 */
package com.android.littleutilities.easystats;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

/**
 * @author alex.veprik
 *
 */
public class DataProvider extends ContentProvider
{
    public static final Uri CONTENT_URI = Uri.parse("content://com.android.littleutilities.easystats.statisticdataprovider");

    public static final String MESSAGES = StatisticDatabaseOpenHelper.TABLE_MESSAGES;
    public static final String CALLS = StatisticDatabaseOpenHelper.TABLE_CALLS;
    public static final String DATA = StatisticDatabaseOpenHelper.TABLE_DATA;

    //DB Tables
    public static final Uri CONTENT_MESSAGES = Uri.parse("content://com.android.littleutilities.easystats.statisticdataprovider/" + MESSAGES);
    public static final Uri CONTENT_CALLS = Uri.parse("content://com.android.littleutilities.easystats.statisticdataprovider/" + CALLS);
    public static final Uri CONTENT_DATA = Uri.parse("content://com.android.littleutilities.easystats.statisticdataprovider/" + DATA);
    
    //Table column names
    public static final String _ID = StatisticDatabaseOpenHelper._ID ; // an ID integer
    public static final String DATE=StatisticDatabaseOpenHelper.DATE; // integer
    public static final String MESSAGE_TYPE = StatisticDatabaseOpenHelper.MESSAGE_TYPE; //integer
    public static final String NUMBER = StatisticDatabaseOpenHelper.NUMBER; //String
    public static final String DIRECTION = StatisticDatabaseOpenHelper.DIRECTION; //integer
    public static final String LENGTH = StatisticDatabaseOpenHelper.LENGTH; //integer
    public static final String IN_SIZE = StatisticDatabaseOpenHelper.IN_SIZE; // integer
    public static final String OUT_SIZE = StatisticDatabaseOpenHelper.OUT_SIZE; // integer
    public static final String TYPE = StatisticDatabaseOpenHelper.TYPE; // integer
    public static final String COUNT = StatisticDatabaseOpenHelper.COUNT; //integer
    public static final String TOTAL_TIME = StatisticDatabaseOpenHelper.TOTAL_TIME; //integer

    public static final int DATA_WIFI = 0;
    public static final int DATA_3G = 1;

    public static final int SMS = 2;
    public static final int MMS = 3;

    public static final int INCOMING = 0;
    public static final int OUTGOING = 1;
    
    private static final String TAG = "StatisticDataProvider";
 
    //DATABASE SUPPORT
    private StatisticDatabaseOpenHelper mDbOpener = null;

    /**
     * Constructor 
     */
    public DataProvider()
    {
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
     */
    @Override
    public int delete(Uri arg0, String arg1, String[] arg2)
    {
        // TODO Auto-generated method stub
        return 0;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#getType(android.net.Uri)
     */
    @Override
    public String getType(Uri uri)
    {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
     */
    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#onCreate()
     */
    @Override
    public boolean onCreate()
    {
        mDbOpener = new StatisticDatabaseOpenHelper(getContext());
        return true;
    }

    private String getTableFromUri(Uri uri)
    {
        return uri.getLastPathSegment();
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder)
    {
        SQLiteDatabase db = mDbOpener.getReadableDatabase();

        if (db != null) {
            SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();

            
            
            Log.e(TAG, "table requested: " + getTableFromUri(uri));

            qBuilder.setTables(getTableFromUri(uri));

            String groupBy = projection[projection.length - 1];
            
            String[] proj = new String[projection.length - 1];

            System.arraycopy(projection, 0, proj, 0, proj.length);

            // Make the query.
            Cursor cur = qBuilder.query(db,
                    proj,
                    selection,
                    selectionArgs,
                    groupBy,
                    null,
                    sortOrder);

            cur.setNotificationUri(getContext().getContentResolver(), uri);

            return cur;
        }

        return null;
    }

    /* (non-Javadoc)
     * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
            String[] selectionArgs)
    {
        // TODO Auto-generated method stub
        return 0;
    }

}
