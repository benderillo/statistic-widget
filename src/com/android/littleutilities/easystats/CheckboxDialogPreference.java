/**
 * 
 */
package com.android.littleutilities.easystats;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.os.Bundle;
import android.os.Message;
import android.preference.ListPreference;
import android.util.AttributeSet;

/**
 * @author alex.veprik
 *
 */
public class CheckboxDialogPreference extends ListPreference
{
    private int mEntries = 0;
    private int mEntryValues = 0;
    private static boolean[] values = {false, false, false};
    private static String[] fields = {DataProvider.MESSAGES, DataProvider.CALLS, DataProvider.DATA};


    public CheckboxDialogPreference(Context context)
    {
        super(context);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param context
     * @param attrs
     */
    public CheckboxDialogPreference(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param context
     * @param attrs
     * @param defStyle
     */
    
     @Override
     public void setEntryValues(int resId)
     {
         mEntryValues = resId;
         super.setEntryValues(resId);
     }

     @Override
     public void setEntries(int resId)
     {
         mEntries = resId;
         super.setEntries(resId);
     }

     public void onDialogClosed(boolean positiveResult)
     {
         super.onDialogClosed(positiveResult);
         for(int i=0; i < values.length; ++i) {
            values[i] = false; 
         }
     }
     
     public void onPrepareDialogBuilder(AlertDialog.Builder builder)
     {
         builder.setMultiChoiceItems(mEntries, values, new OnMultiChoiceClickListener()
         {
            
             @Override
             public void onClick(DialogInterface dialog, int which, boolean isChecked)
             {
                 values[which] = isChecked;
             }
         });
         builder.setPositiveButton(R.string.clear_database, new OnClickListener()
        {
            
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                boolean needToSend = false;
                Bundle data = new Bundle();

                for (int i = 0; i < values.length; ++i) {
                    
                    if (values[i]) {
                        needToSend = true;
                    }

                    data.putBoolean(fields[i], values[i]);

                    if (needToSend) {
                        //Prepare reset structure....
                        Message msg = Message.obtain(null, StatisticDataProvider.MSG_CLEAR_DATABASE);
                        msg.setData(data);

                        ServiceBinder.getInstance(null).doSendMsg(msg);
                    }
                }

                dialog.dismiss();
            }
        });
         
         builder.setNegativeButton(android.R.string.cancel, new OnClickListener()
        {
            
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        });
     }
}
