package com.android.littleutilities.easystats;

import java.util.Calendar;

public final class Util
{
    private Util() {};

    public static final String convertBytes(long bytes)
    {
        String resultStr;

        if ((bytes/(1024*1024*1024)) > 0) { //Gigabytes
            Float giga = ((float)bytes/(1024*1024*1024));
            resultStr = String.format("%.1f", giga) + "Gb";
            

        } else if ((bytes/(1024*1024)) > 0) { //Megabytes
            Float mega = ((float)bytes/(1024*1024));
            resultStr = String.format("%.1f", mega) + "Mb"; //mega.toString() + "Mb";           
        } else if ((bytes/1024) > 0) { //Kilobytes
            Float kilo = ((float)bytes/1024);
            resultStr = String.format("%.1f", kilo) + "Kb";//kilo.toString() + "Kb";
        } else { //bytes
            resultStr = Integer.toString((int) bytes);
        }

        return resultStr;
    }

    public static final String convertSeconds(int seconds)
    {
        Integer minutes = seconds/60;
        Integer hours = minutes/60;
        Integer days = hours/24;
        Integer sec = seconds - minutes*60;
        
        minutes = minutes - hours*60;
        hours = hours - days*24; //now we have proper hours
        
        String s;
        if (sec > 9) 
            s = sec.toString();
        else
            s = "0"+sec.toString();
        
        String m;
        if (minutes > 9) 
            m = minutes.toString();
        else
            m = "0"+minutes.toString();

        String h;
        if (hours > 9) 
            h = hours.toString();
        else
            h = "0"+hours.toString();

        String d;
        if (days > 9) 
            d = days.toString();
        else
            d = "0"+days.toString();

        

        return d+":"+h+":"+m+":"+s;
    }

    public static final int getDateCode(Calendar rightNow)
    {
        int yDay = rightNow.get(Calendar.DAY_OF_YEAR);
        int year = rightNow.get(Calendar.YEAR);

        return Integer.parseInt(String.valueOf(year) + String.valueOf(yDay));
    }
    
    //Date code is YEARDAY. E.g. if today is 2011 and the day number is 100 then the code is 2011100
    public static final int getCurrentDateCode()
    {
        Calendar rightNow = Calendar.getInstance();

        return getDateCode(rightNow);
    }
    
    public static final Calendar getDateFromCode(int code)
    {
        String strCode = String.valueOf(code);
        
        int year = Integer.parseInt(strCode.substring(0, 4));
        int yDay = Integer.parseInt(strCode.substring(4, strCode.length()));
        
        Calendar rightNow = Calendar.getInstance();
        
        rightNow.set(Calendar.YEAR, year);
        rightNow.set(Calendar.DAY_OF_YEAR, yDay);
        
        return rightNow;
    }
}