package com.android.littleutilities.easystats;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.util.Log;

public class Preferences extends PreferenceActivity implements OnSharedPreferenceChangeListener
{
    private Resources mRes = null;
    private static final String TAG = "Preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.tab_activity);
        setPreferenceScreen(createPreferenceHierarchy());
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        getWindow().getDecorView().findViewById(android.R.id.content).setBackgroundResource(R.drawable.window_bg);
        getWindow().getDecorView().findViewById(android.R.id.content).getRootView().setBackgroundResource(R.drawable.window_bg);

    }

    private PreferenceScreen createPreferenceHierarchy() {
        // Root
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        // Dialog based preferences
        PreferenceCategory dialogBasedPrefCat = new PreferenceCategory(this);
        dialogBasedPrefCat.setTitle(R.string.period_title);
        root.addPreference(dialogBasedPrefCat);
        mRes = getResources();

        // List preference
        ListPreference listPref = new ListPreference(this);
        listPref.setEntries(R.array.set_period_options);
        listPref.setEntryValues(R.array.set_period_options);
        listPref.setDialogTitle(R.string.period_title);
        listPref.setKey(Settings.KEY_MESSAGES_PERIOD);
        listPref.setTitle(R.string.messages);        
        listPref.setSummary(Settings.getPreferences(this).getString(Settings.KEY_MESSAGES_PERIOD, mRes.getString(R.string.total)));
        dialogBasedPrefCat.addPreference(listPref);

        listPref = new ListPreference(this);
        listPref.setEntries(R.array.set_period_options);
        listPref.setEntryValues(R.array.set_period_options);
        listPref.setDialogTitle(R.string.period_title);
        listPref.setKey(Settings.KEY_CALLS_PERIOD);
        listPref.setTitle(R.string.Calls);
        listPref.setSummary(Settings.getPreferences(this).getString(Settings.KEY_CALLS_PERIOD, mRes.getString(R.string.total)));
        dialogBasedPrefCat.addPreference(listPref);

        listPref = new ListPreference(this);
        listPref.setEntries(R.array.set_period_options);
        listPref.setEntryValues(R.array.set_period_options);
        listPref.setDialogTitle(R.string.period_title);
        listPref.setKey(Settings.KEY_DATA_PERIOD);
        listPref.setTitle(R.string.data);
        listPref.setSummary(Settings.getPreferences(this).getString(Settings.KEY_DATA_PERIOD, mRes.getString(R.string.total)));
        dialogBasedPrefCat.addPreference(listPref);

        PreferenceCategory DbPrefCat = new PreferenceCategory(this);
        DbPrefCat.setTitle(R.string.db_control_title);
        root.addPreference(DbPrefCat);
        CheckboxDialogPreference pref = new CheckboxDialogPreference(this);
        pref.setEntryValues(R.array.db_reset_list);
        pref.setEntries(R.array.db_reset_list);
        pref.setDialogTitle(R.string.clear_database);
        pref.setTitle(R.string.clear_database);
        pref.setKey(Settings.KEY_CLEAR_DB);        
        DbPrefCat.addPreference(pref);
        
        PreferenceCategory checkboxBasedPrefCat = new PreferenceCategory(this);
        checkboxBasedPrefCat.setTitle(R.string.other_settings_title);
        root.addPreference(checkboxBasedPrefCat);
     // Checkbox preference
        CheckBoxPreference checkboxPref = new CheckBoxPreference(this);
        checkboxPref.setKey(Settings.KEY_CONTACTS_LOOKUP);
        checkboxPref.setTitle(R.string.contacts_lookup_title);
        checkboxPref.setSummary(R.string.lookup_hint);
        checkboxBasedPrefCat.addPreference(checkboxPref);
        
        return root;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1)
    {
        // TODO Auto-generated method stub
        
        if (arg1 != null && !arg1.equals(Settings.KEY_CONTACTS_LOOKUP)) {
            Preference scr = findPreference(arg1);
            if (scr != null) {
                scr.setSummary(Settings.getPreferences(this).getString(arg1, mRes.getString(R.string.total)));
            } else {
                Log.e(TAG, "pref "+ arg1 + " not found");
            }
        }

    }
    
    @Override
    public void onDestroy()
    {
        Settings.getPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }
}
