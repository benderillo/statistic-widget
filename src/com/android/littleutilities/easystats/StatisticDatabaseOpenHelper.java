package com.android.littleutilities.easystats;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

public class StatisticDatabaseOpenHelper extends SQLiteOpenHelper{
    
    public static final String TABLE_MESSAGES="MESSAGES";
    public static final String TABLE_CALLS="CALLS";
    public static final String TABLE_DATA="DATA";

    //Table column names
    public static final String _ID="_id"; // an ID integer
    public static final String DATE="date"; // integer
    public static final String MESSAGE_TYPE="message_type"; //integer
    public static final String NUMBER="number"; //integer
    public static final String DIRECTION="direction"; //integer
    public static final String LENGTH="length"; //integer
    public static final String IN_SIZE="in_size"; // integer
    public static final String OUT_SIZE="out_size"; // integer
    public static final String TYPE="type"; // integer
    public static final String COUNT="count"; //integer
    public static final String KEY="key"; //long
    public static final String TOTAL_TIME="total_time"; //integer


    private static final String TAG="StatisticDatabaseOpenHelper";
    private static final int DATABASE_VERSION = 2;
    protected static final String DATABASE_NAME = "statistics.db";

    
    protected static final String MESSAGES_TABLE_SCHEME = 
            "CREATE TABLE "+ TABLE_MESSAGES +"(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NUMBER + " TEXT,"    //phone number
            + DIRECTION + " INTEGER," //in/out
            + TYPE + " INTEGER,"      //SMS/MMS
            + DATE + " INTEGER,"      //date of transaction
            + COUNT + " INTEGER"     //counter of messages
            + ");";

    protected static final String CALLS_TABLE_SCHEME =
            "CREATE TABLE "+ TABLE_CALLS +"(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + NUMBER + " TEXT,"     //phone number
            + DIRECTION + " INTEGER,"  //in/out
            + DATE + " INTEGER,"       //date of transaction
            + TOTAL_TIME + " INTEGER," //total time spent on calls
            + COUNT + " INTEGER"       //counter of calls
            + ");";


    protected static final String DATA_TABLE_SCHEME =
            "CREATE TABLE "+ TABLE_DATA +"(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TYPE + " INTEGER,"     //wifi/3g/4g
            + DATE + " INTEGER,"       //date of transaction
            + IN_SIZE + " INTEGER,"       //amount of bytes sent/received
            + OUT_SIZE + " INTEGER"       //amount of bytes sent/received
            + ");";
    

    StatisticDatabaseOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "DB is going to be created");

        db.execSQL(MESSAGES_TABLE_SCHEME);
        db.execSQL(CALLS_TABLE_SCHEME);
        db.execSQL(DATA_TABLE_SCHEME);
    }

    public void onOpen(SQLiteDatabase db)
    {
        Log.e(TAG, "db is open, readOnly: "+ db.isReadOnly());
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub
        Log.e(TAG, "DB is being upgraded");
        if (arg0.getVersion() < DATABASE_VERSION) {
            rebuildDatabase(arg0);
        }           
    }
    
    public void rebuildDatabase(SQLiteDatabase db)
    {
        db.execSQL("DROP TABLE "+ TABLE_CALLS);
        db.execSQL("DROP TABLE "+ TABLE_MESSAGES);
        db.execSQL("DROP TABLE "+ TABLE_DATA);
        db.execSQL(MESSAGES_TABLE_SCHEME);
        db.execSQL(CALLS_TABLE_SCHEME);
        db.execSQL(DATA_TABLE_SCHEME);          
    }
    
    public void doDropTable(String table)
    {
        SQLiteDatabase db = getWritableDatabase();

        if (db != null) {
            db.execSQL("DROP TABLE "+ table);;
        }
    }
}
