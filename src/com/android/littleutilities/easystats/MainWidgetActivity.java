package com.android.littleutilities.easystats;


import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TabActivity;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class MainWidgetActivity extends TabActivity implements OnSharedPreferenceChangeListener{
    /** Called when the activity is first created. */
    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    public final static String TAG = "MainWidgetActivity"; 
    
    private TabHost mTabHost;
    private ViewPager mViewPager;
    private TabPagerAdapter mPagerAdapter;
    private boolean mDebug = false;
    private ServiceBinder mService;

    private static final String MY_AD_UNIT_ID = "a14efbd51b427e1";

    private static final String[] TAB_TAGS = {"tabMessages","tabCalls","tabData"};
    
    private static final int[] TAB_LAYOUTS = { R.layout.tab_messages,
                                               R.layout.graph_messages,
                                               R.layout.tab_calls,
                                               R.layout.graph_calls,
                                               R.layout.tab_data,
                                               R.layout.graph_data };
//                                               R.layout.tab_debug };
    
    private LayoutInflater inflater = null;

    private static final int MSG_UPDATE_MESSAGES = 100;
    private static final int MSG_UPDATE_CALLS    = 101;
    private static final int MSG_UPDATE_DATA     = 102;

    private Handler mHandler = new Handler()
    {
        public void handleMessage(Message msg) {

            switch(msg.what)
            {
                case MSG_UPDATE_MESSAGES:
                    //doMessagesUpdate();
                    break;
                case MSG_UPDATE_CALLS:
                    //doCallsUpdate();
                    break;
                case MSG_UPDATE_DATA:
                    //doDataUpdate();
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    };

    private ContentObserver mMessageObserver = new ContentObserver(mHandler)
    {
        public void onChange(boolean selfChange) {
            requestMessagesUpdate();
        }
    };

    private ContentObserver mCallsObserver = new ContentObserver(mHandler)
    {
        public void onChange(boolean selfChange) {
            requestCallsUpdate();
        }
    };

    private ContentObserver mDataObserver = new ContentObserver(mHandler)
    {
        public void onChange(boolean selfChange) {
            requestDataUpdate();
        }
    };

    public final Cursor query (Uri uri, String[] projection, String selection, String[] selectionArgs, String groupBy, String sortOrder)
    {
        String[] proj = new String[projection.length + 1];
        
        System.arraycopy(projection, 0, proj, 0, projection.length);

        // Add GROUP BY construction as the last element of projection 
        proj[projection.length] = groupBy;

        return getContentResolver().query(uri, proj, selection, selectionArgs, sortOrder);
    }    

    private OnClickListener mOnClickListener = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            if (v.getId() == R.id.bClearDb) {
            } else if (v.getId() == R.id.bDumpDb) {
                if (mService != null) {
                    Message msg = Message.obtain(null,
                            StatisticDataProvider.MSG_DUMP_DATABASE);
                    ServiceBinder.getInstance(null).doSendMsg(msg);
                }                
            }
        }
    };

    private class TabPagerAdapter extends PagerAdapter{
        private static final String TAG = "TabPagerAdapter";

        public TabPagerAdapter(MainWidgetActivity activity)
        {
            Log.e(TAG, "TabPagerAdapter instantiated");
        }

        @Override
        public int getCount() {
            return TAB_LAYOUTS.length;
        }

        @Override
        public Object instantiateItem(View collection, int position) {

            Log.e(TAG, "instantiateItem at pos " + position);
            
            View view = View.inflate(getApplicationContext(), TAB_LAYOUTS[position], null);
            
            //Update counters
            if ("tabMessages".equalsIgnoreCase(getTagByIndex(position/2))){
                doMessagesUpdate(view);
            } else if ("tabCalls".equalsIgnoreCase(getTagByIndex(position/2))) {
                doCallsUpdate(view);
            } else if ("tabData".equalsIgnoreCase(getTagByIndex(position/2))) {
                doDataUpdate(view);
            }

            ((ViewPager) collection).addView(view,0);

            return view;
        }

    /**
     * Remove a page for the given position.  The adapter is responsible
     * for removing the view from its container, although it only must ensure
     * this is done by the time it returns from {@link #finishUpdate()}.
     *
     * @param container The containing View from which the page will be removed.
     * @param position The page position to be removed.
     * @param object The same object that was returned by
     * {@link #instantiateItem(View, int)}.
     */
        @Override
        public void destroyItem(View collection, int position, Object view) {
            Log.e(TAG, "destroyItem");
            ((ViewPager) collection).removeView((View)view);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
                return view==((View)object);
        }

        
    /**
     * Called when the a change in the shown pages has been completed.  At this
     * point you must ensure that all of the pages have actually been added or
     * removed from the container as appropriate.
     * @param container The containing View which is displaying this adapter's
     * page views.
     */
        @Override
        public void finishUpdate(View arg0) {}
        

        @Override
        public void restoreState(Parcelable arg0, ClassLoader arg1)
        {
        }

        @Override
        public Parcelable saveState() {
                return null;
        }

        @Override
        public void startUpdate(View arg0) {}

}
    
    ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener()
    {
        
        @Override
        public void onPageSelected(int arg0)
        {
            Log.e(TAG, "onPageSelected " + arg0);
            // TODO Auto-generated method stub
//            Log.e(TAG, mViewPager.getFocusedChild().getTag().toString());
  //          getTabHost().setCurrentTabByTag(mViewPager.getFocusedChild().getTag().toString());
            if (getTabHost().getCurrentTab() != arg0/2) {
                getTabHost().setCurrentTab(arg0/2);               
            }
            //getTabWidget().setT;
        }
        
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2)
        {
            // TODO Auto-generated method stub
            
        }
        
        @Override
        public void onPageScrollStateChanged(int arg0)
        {
            // TODO Auto-generated method stub
            
        }
    };
    
    private String getTagByIndex(int index)
    {
        return TAB_TAGS[index];
    }

    static class DummyTabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public DummyTabFactory(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.e(TAG, "OnCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tab_activity);

        // Create the adView
        AdView adView = new AdView(this, AdSize.BANNER, MY_AD_UNIT_ID);

        // Lookup your LinearLayout assuming it�s been given
        // the attribute android:id="@+id/mainLayout"
        LinearLayout layout = (LinearLayout)findViewById(R.id.main_frame);

        // Add the adView to it
        layout.addView(adView);

        // Initiate a generic request to load it with an ad
        AdRequest req = new AdRequest();
        req.addTestDevice(AdRequest.TEST_EMULATOR);
        req.addTestDevice("9F0539E86F35110040DECDA9A4B5A5E3");
        adView.loadAd(req);

        inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // Let's bind to the service
        mService = ServiceBinder.getInstance(getApplicationContext());
        mService.doBindService();

        //findViewById(R.id.bClearDb).setOnClickListener(mOnClickListener);
        //findViewById(R.id.bDumpDb).setOnClickListener(mOnClickListener);
        //findViewById(android.R.id.tabcontent).setOnTouchListener(onTouchListener);
        
        
        mPagerAdapter = new TabPagerAdapter(this);
        mViewPager = (ViewPager) findViewById(R.id.tab_view_pager);
        
        mViewPager.setOnPageChangeListener(mOnPageChangeListener);
        mViewPager.setAdapter(mPagerAdapter);
        mTabHost = getTabHost();
        
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener()
        {
            
            @Override
            public void onTabChanged(String tabId)
            {
                mViewPager.setCurrentItem(mTabHost.getCurrentTab()*2, true);
            }
        });

        Resources res = getResources();
        
        View mtab = inflater.inflate(R.layout.tab_indicator, null);
        TextView title = (TextView)mtab.findViewById(R.id.tabTitle);
        title.setText(res.getString(R.string.messages));
        ImageView icon = (ImageView)mtab.findViewById(R.id.tabIcon);
        icon.setImageResource(R.drawable.tab_message_selected);
        mTabHost.addTab( mTabHost.newTabSpec("tabMessages").setIndicator(mtab).setContent(new DummyTabFactory(getApplicationContext())) );

        mtab = inflater.inflate(R.layout.tab_indicator, null);
        
        title = (TextView)mtab.findViewById(R.id.tabTitle);
        title.setText(res.getString(R.string.Calls));
        
        icon = (ImageView)mtab.findViewById(R.id.tabIcon);
        icon.setImageResource(R.drawable.tab_calls_selected);

        mTabHost.addTab(mTabHost.newTabSpec("tabCalls").setIndicator(mtab).setContent(new DummyTabFactory(getApplicationContext())));
        
        mtab = inflater.inflate(R.layout.tab_indicator, null);
        
        title = (TextView)mtab.findViewById(R.id.tabTitle);
        title.setText(res.getString(R.string.data));
        icon = (ImageView)mtab.findViewById(R.id.tabIcon);
        icon.setImageResource(R.drawable.tab_data_selected);        
        mTabHost.addTab(mTabHost.newTabSpec("tabData").setIndicator(mtab).setContent(new DummyTabFactory(getApplicationContext())));
        
        if (mDebug) {
            mTabHost.addTab(mTabHost.newTabSpec("tabDebug").setIndicator(res.getString(R.string.debug)).setContent(new DummyTabFactory(getApplicationContext())));
        }

        mTabHost.setCurrentTab(0);

        for (int i = 0; i < mTabHost.getTabWidget().getTabCount(); ++i) {
            mTabHost.getTabWidget().getChildTabViewAt(i).setBackgroundDrawable(res.getDrawable(R.drawable.tab_background_img));
        }

        mTabHost.getTabWidget().setStripEnabled(true);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        
        //Let's register content observers to keep our counters up-to-date
        getContentResolver().registerContentObserver(DataProvider.CONTENT_MESSAGES, true, mMessageObserver);
        getContentResolver().registerContentObserver(DataProvider.CONTENT_CALLS, true, mCallsObserver);
        getContentResolver().registerContentObserver(DataProvider.CONTENT_DATA, true, mDataObserver);
        
        verifyStartTime(Settings.KEY_MESSAGES_START_TIME);
        verifyStartTime(Settings.KEY_CALLS_START_TIME);
        verifyStartTime(Settings.KEY_DATA_START_TIME);
        
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, 
                    AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        Settings.getPreferences(this).registerOnSharedPreferenceChangeListener(this);
        
        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_CANCELED, resultValue);
    }

    private void verifyStartTime(String key)
    {
        long startTime = Settings.getPreferences(this).getLong(key, 0);

        if (startTime == 0) {
            //Put current time into preferences
            Calendar rightNow = Calendar.getInstance();
            startTime = rightNow.getTimeInMillis();

            SharedPreferences.Editor editor = Settings.getPreferences(this).edit();
            editor.putLong(key, startTime);
            editor.commit();
        }
    }

    private void updateFooter(View view, String key)
    {
        TextView mView = (TextView) view.findViewById(R.id.textFooter);

        if (mView != null) {

            long startTime = Settings.getPreferences(this).getLong(key, 0);

            Calendar startDate = Calendar.getInstance();
            
            startDate.setTimeInMillis(startTime);
            Resources res = getResources();
            String since = res.getString(R.string.since);

            mView.setText(since+"<"+startDate.get(Calendar.YEAR)+"/"+startDate.get(Calendar.MONTH)+"/"+startDate.get(Calendar.DAY_OF_MONTH)+">");
        }

    }
    
    //Read info from ContentProvider and Update UI
    private void doMessagesUpdate(View view)
    {
        //For requesting of total count
        String[] projection = {DataProvider.TYPE, DataProvider.DIRECTION, "SUM(" + DataProvider.COUNT + ")"};
        
        final int [][] lookup_table = { {0,0},{0,0},
                                        {R.id.textSmsIn, R.id.textSmsOut}, 
                                        {R.id.textMmsIn, R.id.textMmsOut} }; 
        final int [][] lookup_table1 = { {0,0},{0,0},
                                         {R.id.textSmsInToday, R.id.textSmsOutToday}, 
                                         {R.id.textMmsInToday, R.id.textMmsOutToday} }; 

        
        Cursor cursor = query(DataProvider.CONTENT_MESSAGES, 
                 projection, null,null,DataProvider.TYPE + ", "+DataProvider.DIRECTION, null);

        if (cursor != null) {
            if (cursor.moveToFirst()){
                for (int i = 0; i < cursor.getCount(); ++i) {
                    TextView counter = (TextView) view.findViewById(lookup_table[cursor.getInt(0)][cursor.getInt(1)]);
                    if (counter != null) {
                        counter.setText(cursor.getString(2));
                    }

                    cursor.moveToNext();
                }
            }
            
            cursor.close();
        }
        
       //current date
       long date = Util.getCurrentDateCode();

       cursor = query(DataProvider.CONTENT_MESSAGES, 
                projection, DataProvider.DATE+"="+String.valueOf(date),null,DataProvider.TYPE + ", "+DataProvider.DIRECTION, null);

       if (cursor != null) {
           if (cursor.moveToFirst()){
               for (int i = 0; i < cursor.getCount(); ++i) {
                   TextView counter = (TextView) view.findViewById(lookup_table1[cursor.getInt(0)][cursor.getInt(1)]);
                   if (counter != null) {
                       counter.setText(cursor.getString(2));
                   }
                   cursor.moveToNext();
               }
           }
           
           cursor.close();
       }
       
       //Read the value of when the counter started
       updateFooter(view, Settings.KEY_MESSAGES_START_TIME);
    }

    //Read info from ContentProvider and Update UI
    private void doCallsUpdate(View view)
    {
        //For requesting of total count
        String[] projection = {DataProvider.DIRECTION, "SUM(" + DataProvider.TOTAL_TIME + ")", "SUM(" + DataProvider.COUNT + ")"};
        
        final int [][] lookup_table = {{R.id.textIncomingDuration, R.id.textIncomingCount}, 
                                       {R.id.textOutcoingDuration, R.id.textOutgoingCount} }; 

        final int [][] lookup_table1 = {{R.id.textIncomingDurationToday, R.id.textIncomingCountToday}, 
                                        {R.id.textOutgoingDurationToday, R.id.textOutgoingCountToday} }; 

        
        Cursor cursor = query(DataProvider.CONTENT_CALLS, 
                 projection, null,null,DataProvider.DIRECTION, null);

        if (cursor != null) {
            if (cursor.moveToFirst()){
                for (int i = 0; i < cursor.getCount(); ++i) {
                    TextView time = (TextView) view.findViewById(lookup_table[cursor.getInt(0)][0]);
                    TextView counter = (TextView) view.findViewById(lookup_table[cursor.getInt(0)][1]);

                    if (time != null) {
                        time.setText(Util.convertSeconds(cursor.getInt(1)));
                    }

                    if (counter != null) {
                        counter.setText(cursor.getString(2));
                    }

                    cursor.moveToNext();
                }
            }
            
            cursor.close();
        }
        
        long date = Util.getCurrentDateCode();

        cursor = query(DataProvider.CONTENT_CALLS, 
                projection, DataProvider.DATE+"="+String.valueOf(date),null,DataProvider.DIRECTION, null);

       if (cursor != null) {
           if (cursor.moveToFirst()){
               for (int i = 0; i < cursor.getCount(); ++i) {
                   TextView time = (TextView) view.findViewById(lookup_table1[cursor.getInt(0)][0]);
                   TextView counter = (TextView) view.findViewById(lookup_table1[cursor.getInt(0)][1]);

                   if (time != null) {
                       time.setText(Util.convertSeconds(cursor.getInt(1)));
                   }

                   if (counter != null) {
                       counter.setText(cursor.getString(2));
                   }

                   cursor.moveToNext();
               }
           }
           
           cursor.close();
       }

       updateFooter(view, Settings.KEY_CALLS_START_TIME);
    }

    //Read info from ContentProvider and update UI
    private void doDataUpdate(View view)
    {
        //For requesting of total count
        String[] projection = {"SUM(" + DataProvider.IN_SIZE + ")", "SUM(" + DataProvider.OUT_SIZE + ")"};

        Cursor cursor = query(DataProvider.CONTENT_DATA, 
                 projection, null,null,null, null);

        if (cursor != null) {
            if (cursor.moveToFirst()){
                TextView counterIn = (TextView) view.findViewById(R.id.textInData);
                TextView counterOut = (TextView) view.findViewById(R.id.textOutData);

                if (counterIn != null) {
                    counterIn.setText(Util.convertBytes(cursor.getInt(0)));
                }

                if (counterOut != null) {
                    counterOut.setText(Util.convertBytes(cursor.getInt(1)));
                }
            }
            
            cursor.close();
        }
        
        long date = Util.getCurrentDateCode();

        cursor = query(DataProvider.CONTENT_DATA, 
                projection, DataProvider.DATE+"="+String.valueOf(date),null,null, null);

        if (cursor != null) {
           if (cursor.moveToFirst()){
               TextView counterIn = (TextView) view.findViewById(R.id.textInDataToday);
               TextView counterOut = (TextView) view.findViewById(R.id.textOutDataToday);

               if (counterIn != null) {
                   counterIn.setText(Util.convertBytes(cursor.getInt(0)));
               }

               if (counterOut != null) {
                   counterOut.setText(Util.convertBytes(cursor.getInt(1)));
               }
           }
           
           cursor.close();
        }

        updateFooter(view, Settings.KEY_DATA_START_TIME);
    }

    public void onResume() {
        Log.e(TAG, "OnResume");
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuItem mItem = null;

        mItem = menu.add(R.string.settings);
        mItem.setIcon(android.R.drawable.ic_menu_preferences).setOnMenuItemClickListener(new OnMenuItemClickListener()
        {
            
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                //startActivityForResult(new Intent(getBaseContext(), SettingsActivity.class), 1);
                startActivityForResult(new Intent(getBaseContext(), Preferences.class), 1);
                return true;
            }
        });

        mItem = menu.add(R.string.about).setIcon(android.R.drawable.ic_menu_info_details).setOnMenuItemClickListener(new OnMenuItemClickListener()
        {            
            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                showDialog(0);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected Dialog onCreateDialog( int id )
    {
        return new AlertDialog.Builder(this).
        setCancelable(true).setMessage(R.string.about_message).
        setTitle(R.string.about).setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
            }
        }) .create();
    }


    public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1)
    {
        // TODO Auto-generated method stub
        if (arg1 != null) {
            if (arg1.equals(Settings.KEY_CONTACTS_LOOKUP)) {
                //arg0.getBoolean(arg1, false);
            } else {
                //Need to restart alarms
                // Alarms are not preserved after phone reboot so let's keep them in the service side
                // The service is restarted after re-boot, it will re-enable alarms for us
                
            }
        }
    }
    
    public void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();

        Intent resultValue = new Intent();
        resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
        setResult(RESULT_OK, resultValue);

        //finish();
    }

    public void onDestroy() {
        Log.e(TAG, "onDestroy");

        getContentResolver().unregisterContentObserver(mMessageObserver);
        getContentResolver().unregisterContentObserver(mCallsObserver);
        getContentResolver().unregisterContentObserver(mDataObserver);

        Settings.getPreferences(this).unregisterOnSharedPreferenceChangeListener(this);

        mService.doUnbindService();
        mViewPager = null;
        mPagerAdapter = null;

        super.onDestroy();
    }
    
    private void requestMessagesUpdate()
    {
        mHandler.removeMessages(MSG_UPDATE_MESSAGES);
        mHandler.sendEmptyMessage(MSG_UPDATE_MESSAGES);
    }

    private void requestCallsUpdate()
    {
        mHandler.removeMessages(MSG_UPDATE_CALLS);
        mHandler.sendEmptyMessage(MSG_UPDATE_CALLS);
    }

    private void requestDataUpdate()
    {
        mHandler.removeMessages(MSG_UPDATE_DATA);
        mHandler.sendEmptyMessage(MSG_UPDATE_DATA);
    }
}