package com.android.littleutilities.easystats;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

public final class Settings
{
    public static SharedPreferences mPreferences;

    public final static String KEY_CONTACTS_LOOKUP = "pref_contacts_lookup";
    public final static String KEY_MESSAGES_PERIOD = "pref_messages_period";
    public final static String KEY_CALLS_PERIOD = "pref_calls_period";
    public final static String KEY_DATA_PERIOD = "pref_data_period";
    public final static String KEY_CLEAR_DB = "pref_clear_db";

    public final static String KEY_MESSAGES_START_TIME = "pref_messages_start_time";
    public final static String KEY_CALLS_START_TIME = "pref_calls_start_time";
    public final static String KEY_DATA_START_TIME = "pref_data_start_time";

   
    public final static int VAL_TOTAL = 0;
    public final static int VAL_DAILY = 1;
    public final static int VAL_WEEKLY = 2;
    public final static int VAL_MOTHLY = 3;
    public final static int[] mTextMap = {R.string.total, R.string.daily, R.string.weekly, R.string.monthly};
    
    
    //public final static HashMap<String, int[]> mMap; 

    public final static int ITEM_INDEX_MESSAGES = 0;
    public final static int ITEM_INDEX_CALLS = 1;
    public final static int ITEM_INDEX_DATA = 2;

    public final static String[] mItemsKeyMap = {KEY_MESSAGES_PERIOD, KEY_CALLS_PERIOD, KEY_DATA_PERIOD};
    
    public static final SharedPreferences getPreferences(Context context) {
        if(Settings.mPreferences == null) {
            Settings.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return Settings.mPreferences;
    }
    

    
}
